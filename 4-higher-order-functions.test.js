describe("higher order functions", () => {
  it('functions accepting other functions as parameters', () => {
    const greetInformal = () => console.log("Howdy!")
    const greetFormal = () => console.log("Welcome")

    const doGreeting = (formal, informal, type) => {
      if(type === "formal") {
        formal()
      }
      else {
        informal()
      }
    }

    doGreeting(greetFormal, greetInformal, "informal")
  })

  it("array's higher order functions", () =>{
    const array = [1,2,3,4,5]

    expect(array.map(x => x + 2)).toEqual([3,4,5,6,7])
    expect(array.filter(x => x>2.5)).toEqual([3,4,5])
    expect(array.reduce((a, b) => a + b)).toEqual(15)

    // they don't mutate original array
    expect(array).toEqual([1,2,3,4,5])

  })

  describe("Exercise!", () => {
    // Look at those two recursive functions
    // Do they look similar to each other?
    function sum(elements) {
      if(elements.length === 0) {
        return 0
      }

      return elements[0] + sum(elements.slice(1))
    }

    function product(elements) {
      if(elements.length === 0) {
        return 1
      }

      return elements[0] * product(elements.slice(1))
    }

    // Your task is to create a higher order function to extract
    // the common logic
    it('should do operation given to the higher order function', () => {
      const array = [1,2,3,4,5]
      const _ = "something"

      const doOperation = () => {}

      const sumOfElements = doOperation(_, _, array)
      const productOfElements = doOperation(_, _, array)

      expect(sumOfElements).toEqual(15)
      expect(productOfElements).toEqual(120)
    })

    // Bonus points
    it('can you write `map` function using your previous doOperation function?', () => {
      const array = [1, 2, 3, 4]
      const _ = "something"
      const toTens = x => x * 10

      const doOperation = () => {}
      const map = (f, array) => doOperation(
        _,
        _,
        array
      )

      expect(map(toTens, array)).toEqual([10, 20, 30, 40])
    })

    // super cool bonus points
    it('use tail recursion', () => {
      const array = [1, 2, 3, 4, 5]
      const _ = "something"

      const doOperation = () => {}

      expect(doOperation(0, (a, b) => a + b, array)).toEqual(15)
      expect(doOperation(1, (a, b) => a * b, array)).toEqual(120)
    })
  })
})