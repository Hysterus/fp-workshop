const lowerCase = str => str.toLowerCase()
const split = separator => str => str.split(separator)
const join = separator => arr => arr.join(separator)

const splitAtSpace = split(' ')
const joinWithHyphen = join('-')

const stringToSlugify = "Fun with Functions"


describe("exercise!", () => {
  it("write your own pipe function", () => {
    const pipeTwo = (f, g) => x => g(f(x))
    const customPipe = (...fns) => fns.reduce(pipeTwo)

    const slugify = customPipe(
      lowerCase,
      splitAtSpace,
      joinWithHyphen,
    )

    const slug = slugify(stringToSlugify)

    expect(slug).toEqual('fun-with-functions')
  })
})