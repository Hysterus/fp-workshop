import {identity, memoizeWith} from "ramda"

describe("functions in FP are first class objects", () => {
  it("can be assigned to variable", () => {
    var someFunc = function add(a, b) {
      return a + b
    }

    expect(someFunc(1, 2)).toEqual(3)
  })


  it("can even have some fields", () => {
    function add(a, b) {
      return a + b
    }

    add.testField = 123

    expect(add.testField).toEqual(123)
  })


  it('can be written with arrow notation', () => {
    const add = (a, b) => a + b

    expect(add(1, 2)).toEqual(3)
  })


  it('can be passed as a parameter', () => {
    const add = (a, b) => a + b

    const sum = [1, 2, 3].reduce(add, 0)

    expect(sum).toEqual(6)
  })

  it('can call itself recursively', () => {
    const factorial = (x) => {
      if (x === 1) {
        return x
      }

      return x * factorial(x - 1)
    }

    expect(factorial(10)).toEqual(3628800)
  })

  // BONUS
  describe("tail call optimization", () => {
    it("can exceed call stack", () => {
      const factorial = (x) => {
        if (x === 1) {
          return x
        }

        return x * factorial(x - 1)
      }

      expect(factorial(10000000)).toEqual(expect.anything())
    })

    it("it should not exceed call stack", () => {
      const factorial = (x, accumulator = x) => {
        if (x === 1) {
          return accumulator
        }

        return factorial(x - 1, accumulator * (x - 1))
      }

      expect(factorial(10)).toEqual(3628800)
      expect(factorial(10000000)).toEqual(expect.anything())
    })
  })
})