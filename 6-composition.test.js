import {compose, filter, map, pipe, prop} from "ramda"
import {krakens} from "./krakens"

const lowerCase = str => str.toLowerCase()
const split = separator => str => str.split(separator)
const join = separator => arr => arr.join(separator)

const splitAtSpace = split(' ')
const joinWithHyphen = join('-')


const stringToSlugify = "Fun with Functions"

describe('functional composition', () => {
  it("may be complicated", () => {
      const slug = joinWithHyphen(splitAtSpace(lowerCase(stringToSlugify)))

      expect(slug).toEqual('fun-with-functions')
    }
  )

  it('can be simplified', () => {
    const slugify = str => joinWithHyphen(splitAtSpace(lowerCase(str)))

    const slug = slugify(stringToSlugify)

    expect(slug).toEqual('fun-with-functions')
  })

  it('can be simplified even more', () => {
    const slugify = compose(
      joinWithHyphen,
      splitAtSpace,
      lowerCase
    )

    const slug = slugify(stringToSlugify)

    expect(slug).toEqual('fun-with-functions')
  })

  it('if you dont like the order, change it', () => {
    const slugify = pipe(
      lowerCase,
      splitAtSpace,
      joinWithHyphen,
    )

    const slug = slugify(stringToSlugify)

    expect(slug).toEqual('fun-with-functions')
  })


})

describe("exercise!", () => {
  it("write your own pipe function", () => {
    // TIP
    // pipe function type:
    // ((a -> b), (b -> c), (c -> d)) -> a -> d
    const customPipe = (...fns) => {
    }

    const slugify = customPipe(
      lowerCase,
      splitAtSpace,
      joinWithHyphen,
    )

    const slug = slugify(stringToSlugify)

    expect(slug).toEqual('fun-with-functions')
  })
})

describe("compisition works great with currying", () => {
  it('cool operations on arrays', () => {
    const getBackendersNames = pipe(
      filter(kraken => kraken.end === "back"),
      map(prop('name')),
      map(lowerCase),
    )

    expect(getBackendersNames(krakens)).toEqual([
      "szczepan",
      "dawid",
      "koxpower",
      "hysterus1",
      "gaurav",
      "skiera",
      "paweł",
      "marcin",
      "ania",
      "marek",
    ])
  })

  it('without currying', () => {
    const getFrontendersNames = developers => pipe(
      array => filter(kraken => kraken.end === "front", array),
      array => map(obj => prop('name', obj), array),
      array => map(lowerCase, array)
    )(developers)

    expect(getFrontendersNames(krakens)).toEqual([
      "wojtek",
    ])
  })

})