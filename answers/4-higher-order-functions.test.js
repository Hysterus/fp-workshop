describe("Exercise!", () => {
  // Look at those two recursive functions
  // Do they look similar to each other?
  function sum(elements) {
    if (elements.length === 0) {
      return 0
    }

    return elements[0] + sum(elements.slice(1))
  }

  function product(elements) {
    if (elements.length === 0) {
      return 1
    }

    return elements[0] * product(elements.slice(1))
  }

  // Your task is to create a higher order function to extract
  // the common logic
  it('should do operation given to the higher order function', () => {
    const array = [1, 2, 3, 4, 5]
    const _ = "something"

    const doOperation = (initial, f, array) => {
      if (array.length === 0) {
        return initial
      }

      return f(array[0], doOperation(initial, f, array.slice(1)))
    }

    expect(doOperation(0, (a, b) => a + b, array)).toEqual(15)
    expect(doOperation(1, (a, b) => a * b, array)).toEqual(120)
  })

  // Bonus points
  it('can you write `map` function using your previous doOperation function?', () => {
    const array = [1, 2, 3, 4]
    const toTens = x => x * 10
    const doOperation = (initial, f, array) => {
      if (array.length === 0) {
        return initial
      }

      return f(array[0], doOperation(initial, f, array.slice(1)))
    }

    const map = (f, array) => doOperation(
      [],
      (e, acc) => [f(e), ...acc],
      array
    )

    expect(map(toTens, array)).toEqual([10, 20, 30, 40])
  })

  // super cool bonus points
  it('use tail recursion', () => {
    const array = [1, 2, 3, 4, 5]
    const _ = "something"

    const doOperation = (initial, f, array) => {
      if (array.length === 0) {
        return initial
      }

      return doOperation(f(initial, array[0]), f, array.slice(1))
    }

    expect(doOperation(0, (a, b) => a + b, array)).toEqual(15)
    expect(doOperation(1, (a, b) => a * b, array)).toEqual(120)
  })
})