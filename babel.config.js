const presets = [
  [
    "@babel/env",
    {
      useBuiltIns: "usage",
      corejs: 3
    },
  ],
]

const plugins = ["tailcall-optimization"]


module.exports = {presets, plugins}