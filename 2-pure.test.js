import {sleep} from "./utils"

describe("Functions in FP are (most of the time) pure", () => {
  it("functions should not cause side effects", () => {
    let counter = 1

    // this is not pure
    function increase(by) {
      counter += by
    }

    increase(10)

    expect(counter).toEqual(11)
  })

  it('should be "referentially transparent" - always return the same value for the same params', () => {
    const PI = 3.14
    const calculateArea = (radius) => PI * radius * radius

    expect(calculateArea(10)).toEqual(314)
    expect(calculateArea(10)).toEqual(314)
    expect(calculateArea(10)).toEqual(314)
  })


  it('example of a function returning different values for the same params', () => {
    let PI = 3.14
    const calculateArea = (radius) => PI * radius * radius

    expect(calculateArea(10)).toEqual(314)
    PI = 10
    expect(calculateArea(10)).not.toEqual(314)
  })

  describe("Examples and signs of impurity", () => {
    it('mutations', () => {
      const array = [1,2,3]

      array.reverse()

      expect(array).toEqual([3,2,1])
    })

    it('reassignments', () => {
      let array = [1,2,3]

      const f = () => array = 5
      f()

      expect(array).toEqual(5)
    })

    it('time', async () => {
      const firstDate = Date.now()

      await sleep(500)

      expect(firstDate).not.toEqual(Date.now())
    })

    it('and more', () => {
      // observable side effects, ie. printing to console
      console.log("hello my dude")

      // if statements
      if(2+2 === 4) {
        console.log("hello again my dude")
      }

      // loops
      const array = [1,2,3,4]
      let sum = 0
      for(let i = 0; i < array.length ; ++i) {
        sum += array[i]
      }

      // Randomness
      console.log(Math.random())
    })
  })
})