describe("FP favors immutability", () => {
  it("const instead of let", () => {
    const numberTwo = 2
    let numberThree = 3

    numberThree = 4
    numberTwo = 5
  })

  it('no array mutations', () => {
    const mutatedArray = [1, 2, 3, 4]
    const array = [1, 2, 3, 4]

    // instead of
    mutatedArray.push(5)

    // do
    const newArray = array.concat(5)
    // or
    const newArray2 = [...array, 5]

    expect(newArray).toEqual([1, 2, 3, 4, 5])
    expect(newArray2).toEqual([1, 2, 3, 4, 5])
    expect(array).toEqual([1, 2, 3, 4])
    expect(mutatedArray).toEqual([1, 2, 3, 4, 5])
  })

  it('no object mutations', () => {
    const mutatedObject = {
      a: 10
    }
    const obj = {
      a: 10
    }

    // instead of
    mutatedObject.b = 20
    delete mutatedObject.a

    // do
    const newObj = Object.assign({}, obj, {a: undefined, b: 20})
    // or
    const newObjSpread = {...obj, a: undefined, b: 20}

    expect(newObj).toEqual({b: 20})
    expect(newObjSpread).toEqual({b: 20})
    expect(obj).toEqual({a: 10})

    expect(mutatedObject).toEqual({b: 20})
  })

  it("expressions instead of statements", () => {
    const today = new Date().getDay()

    // instead of:
    let meal

    if(today === 5) {
      meal = "vegan pizza"
    }
    else {
      meal = "potatoes"
    }

    // do:
    const immutableMeal = today === 5 ? "vegan pizza" : "potatoes"
  })
})

describe("Exercise!", () => {
  // Write a function that will calculate sum of elements of an array
  // without: any mutations, reassignments or map/filter/reduce methods
  // bonus points: use tail call optimization
  it('should calculate sum of array', () => {
    const array = [1, 2, 3, 4, 5, 6, 7, 8]

    function calculateSum() {

    }

    expect(calculateSum(array)).toEqual(36)
  })
})