describe("Exercise!", () => {
  // Write a function that will calculate a sum of elements of an array
  // without: any mutations, reassignments or map/filter/reduce methods
  it('should calculate sum of array', () => {
    const array = [1, 2, 3, 4, 5, 6, 7, 8]

    function calculateSum(array, accumulator = 0) {
      if(array.length === 0) {
        return accumulator
      }

      return calculateSum(array.slice(1), accumulator + array[0])
    }

    expect(calculateSum(array)).toEqual(36)
  })
})