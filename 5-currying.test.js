import {curry, filter} from "ramda"

describe('currying', () => {
  it('can be called "twice"', () => {
    const add = function (a) {
      return function (b) {
        return a + b
      }
    }

    expect(add(5)(10)).toEqual(15)
  })

  it('can be curried way easier', () => {
    const add = a => b => a + b

    expect(add(5)(10)).toEqual(15)
  })

  it('can be used to have a partial application', () => {
    const add = a => b => a + b

    const increment = add(1)

    expect(increment(100)).toEqual(101)
  })

  it('can be auto-curried', () => {
    const add = curry((a, b) => a + b)

    const increment = add(1)

    expect(add(5, 10)).toEqual(15)
    expect(add(5)(10)).toEqual(15)
    expect(increment(100)).toEqual(101)
  })

  it('lazy evaluation', () => {
    const printLine = text => () => console.log(text)

    const doSideEffect = printLine("Hello")

    doSideEffect()
  })

  it("ramdajs methods are automatically curried", () => {
    const arr = [1, 2, 3, 4, 5]

    const filterGreaterThan4 = filter(x => x > 4)

    expect(filterGreaterThan4(arr)).toEqual([5])
  })

  // Tip:
  // Even in javascript, Haskell's type notation for functions is very popular when
  // describing functions related to FP
  // In example:
  // filter :: (a -> boolean) -> [a] -> [a]
})